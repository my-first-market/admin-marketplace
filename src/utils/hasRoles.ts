import React, { ReactElement, useMemo } from 'react'

interface IProps {
  children: ReactElement,
  roles: string[],
}

const HasRoles = ({ children, roles, ...rest }: IProps) => {
  const localRoles = localStorage.getItem('permissions')
  console.log(localRoles)
  const userRoles: any = []
  userRoles.push(localRoles)
  const hasAccess = useMemo(() => {
    return userRoles.some((r: any) => roles.includes(r))
  }, [roles, userRoles])
  return (
    hasAccess ? React.cloneElement(children, { ...rest }) : null
  )
}

export default HasRoles
