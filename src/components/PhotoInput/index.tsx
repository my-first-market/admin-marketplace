import uploadPhotos from 'api/uploadImage'
import { addField } from 'ra-core'
import React, { useCallback } from 'react'
import { ImageField, ImageInput } from 'react-admin'
import { FieldRenderProps } from 'react-final-form'
import handleError from 'api/error'

interface IProps extends FieldRenderProps<File, HTMLInputElement> {
  label?: string,
}

const PhotoInput = ({ label, input: { onChange } }: IProps) => {
  const handleChange = useCallback((image) => {
    if (!image) { return onChange(undefined) }
    uploadPhotos([image])
    .then((res) => onChange(res.data[0].src))
    .catch((error) => {
      handleError(error)
    })
  }, [onChange])
  return (
  <ImageInput
    source='image'
    label={label}
    multiple={false}
    accept='.jpeg, .jpg, .png'
    onChange={handleChange}
    >
    <ImageField source='image.url' title='Preview' />
  </ImageInput>)
}

export default addField(PhotoInput)
