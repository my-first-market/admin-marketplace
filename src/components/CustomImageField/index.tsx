import get from 'lodash/get';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import sanitizeProps from './sanitizeProps';
import { FieldProps } from './types';

const useStyles = makeStyles(
    {
        list: {
            display: 'flex',
            listStyleType: 'none',
        },
        image: {
            margin: '0.5rem',
            maxHeight: '10rem',
        },
    },
    { name: 'RaImageField' }
);

interface Props {
    source?: string,
}

interface IPassedProps {
    basePath?: string,
    record?: IRecord,
    resource?: string,
    source: string,
    src?: string,
}


const ImageField = (props: Props & IPassedProps)  => {
    const sourceValue = get(props.record, props.source);
    const classes = useStyles();
    if (!sourceValue) {
        return <div />;
    }

    if (Array.isArray(sourceValue)) {
        return (
            <ul
                className={classes.list}
            >
                {sourceValue.map((file, index) => {
                    const srcValue = get(file, props.src || '')

                    return (
                        <li key={index}>
                            <img
                                src={`${process.env.REACT_APP_API_PATH}/${srcValue}`}
                                className={classes.image}
                            />
                        </li>
                    );
                })}
            </ul>
        );
    }


    return (
        <div>
            <img
                src={`${process.env.REACT_APP_API_PATH}/${sourceValue}`}
                className={classes.image}
            />
        </div>
    );
};

export default ImageField