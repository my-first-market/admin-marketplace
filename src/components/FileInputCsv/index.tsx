import uploadPhotos from 'api/uploadImage'
import { addField } from 'ra-core'
import React, { useCallback, useState } from 'react'
import { FileField, FileInput } from 'react-admin'
import { FieldRenderProps } from 'react-final-form'
import handleError from 'api/error'
import uploadFileCsv from 'api/uploadFileCsv'

interface IProps extends FieldRenderProps<File, HTMLInputElement> {
  label?: string,
  accept: string
}

const FileInputCsv = ({ label, accept, input: { onChange } }: IProps) => {
  const handleChange = useCallback((file) => {
    if (!file) { return onChange(undefined) }
    uploadFileCsv([file])
    .then((res) => onChange(res.data.csv))
    .catch((error) => {
      handleError(error)
    })
  }, [onChange])

  return (
  <FileInput
    source='csv'
    label={label}
    multiple={false}
    accept={accept}
    onChange={handleChange}
    >
    <FileField source='csv_url' title='Preview' />
  </FileInput>)
}

export default addField(FileInputCsv)
