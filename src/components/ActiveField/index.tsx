import { ListItemIcon } from '@material-ui/core'
import StarIcon from '@material-ui/icons/Star'
import CloseIcon from '@material-ui/icons/Close'
import CheckIcon from '@material-ui/icons/Check'
import fixCloneComponentProps from 'enhacers/fixCloneComponentProps'
import { get } from 'lodash'
import React from 'react'

interface IProps {
  source?: string,
  label?: string,
}

interface IPassedProps {
  basePath: string,
  record: any,
  resource: string,
  source: string,
}

const ActiveField = ({record, source}: IProps & IPassedProps) => {
  const value = get(record, source)
  return (
    <>
      <ListItemIcon>
        { value 
        ? <CheckIcon />
        : <CloseIcon /> }
      </ListItemIcon>
    </>
  )
}

export default fixCloneComponentProps<IProps>(ActiveField)
