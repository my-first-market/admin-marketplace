import React from "react"
import { ImageField} from 'react-admin'
import fixCloneComponentProps from "enhacers/fixCloneComponentProps"
import { get, mapKeys } from "lodash"

interface IProps {
  source?: string,
}

interface IPassedProps {
  basePath: string,
  record: IRecord,
  resource: string,
  source: string,
}

const ImageFieldsTest = (props: IProps & IPassedProps) => {
  const value = get(props.record, props.source)
  console.log(value)
  return (
    <ImageField source={`${process.env.REACT_APP_API_PATH}/${value}`} src={`${process.env.REACT_APP_API_PATH}/${value}`} {...props} />
  )
}
export default fixCloneComponentProps<IProps>(ImageFieldsTest)