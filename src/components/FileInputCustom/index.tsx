import uploadPhotos from 'api/uploadImage'
import { addField } from 'ra-core'
import React, { useCallback, useState } from 'react'
import { FileField, FileInput } from 'react-admin'
import { FieldRenderProps } from 'react-final-form'
import handleError from 'api/error'
import uploadFile from 'api/uploadFile'

interface IProps extends FieldRenderProps<File, HTMLInputElement> {
  label?: string,
  accept: string
}

const FileInputCustom = ({ label, source, accept, csv, input: { onChange } }: IProps) => {
  const handleChange = useCallback((file) => {
    if (!file) { return onChange(undefined) }
    csv ? uploadFile([file]) : uploadFile([file])
    .then((res) => onChange(res.data.images))
    .catch((error) => {
      handleError(error)
    })
  }, [onChange])
  return (
  <FileInput
    source='images'
    label={label}
    multiple={false}
    accept={accept}
    onChange={handleChange}
    >
    <FileField source='images_url' title='Preview' />
  </FileInput>)
}

export default addField(FileInputCustom)
