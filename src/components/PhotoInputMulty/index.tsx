import uploadPhotos from 'api/uploadImage'
import { addField } from 'ra-core'
import React, { useCallback } from 'react'
import { ImageField, ImageInput } from 'react-admin'
import { FieldRenderProps } from 'react-final-form'
import handleError from 'api/error'

interface IProps extends FieldRenderProps<File, HTMLInputElement> {
  label?: string,
}

const PhotoInputMulty = ({ label, input: { onChange } }: IProps) => {
  const handleChange = useCallback((images) => {
    if (!images) { return onChange(undefined) }
    uploadPhotos(images)
    .then((res) => onChange(res.data.map((item: any) => item.src)))
    .catch((error) => {
      handleError(error)
    })
  }, [onChange])
  return (
  <ImageInput
    source='images'
    label={label}
    multiple={true}
    accept='.jpeg, .jpg, .png'
    onChange={handleChange}
    >
    <ImageField source='images.url' title='Preview' />
  </ImageInput>)
}

export default addField(PhotoInputMulty)
