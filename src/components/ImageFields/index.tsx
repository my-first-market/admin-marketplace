import React from "react"
import { ImageField} from 'react-admin'
import fixCloneComponentProps from "enhacers/fixCloneComponentProps"
import { get, mapKeys } from "lodash"

interface IProps {
  source?: string,
}

interface IPassedProps {
  basePath: string,
  record: IRecord,
  resource: string,
  source: string,
}

const ImageFields = (props: IProps & IPassedProps) => {
  const value = get(props.record, props.source)
  console.log(value)
  return (
    <ImageField source='gallery' src='url' />
  )
}
export default fixCloneComponentProps<IProps>(ImageFields)