import React, { ComponentType } from 'react'

function fixCloneComponentProps<T>(Component: any): ComponentType<T> {
  return (props: any) => (
    <Component {...props} />
  )
}

export default fixCloneComponentProps
