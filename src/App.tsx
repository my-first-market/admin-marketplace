import React from 'react';
import './App.css';
import Admin from 'widgets/Admin'
import { Resource, usePermissions, ShowGuesser } from 'react-admin'
import ShopList from 'resources/shops/ShopList';
import ShopCreate from 'resources/shops/ShopCreate';
import UserList from 'resources/users/UserList';
import UserEdit from 'resources/users/UserEdit'
import UserCreate from 'resources/users/UserCreate';
import ShopEdit from 'resources/shops/ShopEdit';
import ProductsList from 'resources/products/ProductList';
import ProductEdit from 'resources/products/ProductEdit';
import ProductCreate from 'resources/products/ProductCreate';
import CurrencyList from 'resources/currency/CurrencyList';
import CurrencyEdit from 'resources/currency/CurrencyEdit';
import CurrencyCreate from 'resources/currency/CurrencyCreate';
import OrderList from 'resources/orders/OrderList';
import OrderEdit from 'resources/orders/components/OrderEdit'
import ReviewsList from 'resources/reviews/ReviewsList';
import ReviewsEdit from 'resources/reviews/ReviewsEdit';
import LeadList from 'resources/leads/LeadList';
import LeadEdit from 'resources/leads/LeadEdit';
import ImportProductCreate from 'resources/importProducts/ImportProductCreate';
import ImportList from 'resources/importProducts/importList';


const App: React.FC  = () => {
  return (
    <Admin>
      <Resource name='shops' list={ShopList} create={ShopCreate} edit={ShopEdit} />
      <Resource name='users' list={UserList} edit={UserEdit} create={UserCreate} />
      <Resource name='products' list={ProductsList} edit={ProductEdit} create={ProductCreate} />
      <Resource name='currencies' list={CurrencyList} edit={CurrencyEdit} create={CurrencyCreate} />
      <Resource name='orders' list={OrderList} edit={OrderEdit}/>
      <Resource name='reviews' list={ReviewsList} edit={ReviewsEdit}/>
      <Resource name='leads' list={LeadList} edit={LeadEdit}/>
      <Resource name='import/products' create={ImportProductCreate} list={ImportList} />
    </Admin>
  );
}

export default App;
