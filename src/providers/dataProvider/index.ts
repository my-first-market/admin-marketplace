import { DataProvider } from 'ra-core'
import build from './builder'
import {
  shops,
  users,
  currencies,
  products,
  orders,
  images,
  reviews,
} from './resources'

type ProvidersType = Record<string, DataProvider>

const providers: ProvidersType = {
  shops,
  users,
  currencies,
  orders,
  products,
  images,
  reviews,
  default: build(),
}

function getProviderFor(resource: string): DataProvider {
  return (resource in providers) ? providers[resource] : providers.default
}

const dataProvider: DataProvider = {
  getList: (resource, params) => getProviderFor(resource).getList(resource, params),
  getOne: (resource, params) => getProviderFor(resource).getOne(resource, params),
  getMany: (resource, params) => getProviderFor(resource).getMany(resource, params),
  getManyReference: (resource, params) => getProviderFor(resource).getManyReference(resource, params),
  update: (resource, params) => getProviderFor(resource).update(resource, params),
  updateMany: (resource, params) => getProviderFor(resource).updateMany(resource, params),
  create: (resource, params) => getProviderFor(resource).create(resource, params),
  delete: (resource, params) => getProviderFor(resource).delete(resource, params),
  deleteMany: (resource, params) => getProviderFor(resource).deleteMany(resource, params),
}

export default dataProvider
