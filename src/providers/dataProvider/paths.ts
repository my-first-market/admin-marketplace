import { ActionType } from './types'

const NESTED_ACTIONS = [
  'getList',
  'getMany',
  'getManyReference',
  'updateMany',
  'deleteMany',
  'create',
]

function apiPath(_: ActionType): string {
  return ''
}

function imageApi(action: ActionType): string {
  return NESTED_ACTIONS.includes(action)
  ? `https://api.myeden.xyz/`
  : `https://api.myeden.xyz/`
}

export {
  apiPath,
  imageApi
}
