import isNumber from 'lodash/isNumber'

interface INormalizedData {
  data: any,
  total?: number,
}

interface IReference {
  type: string,
  id: string
}

interface IJsonApiEntity {
  id: string,
  type: string,
  attributes: {
    [key: string]: any,
  },
  relationships: {
    [key: string]: {
      data?: IReference | IReference[],
    },
  }
}

interface IJsonApiResponse {
  data: IJsonApiEntity | IJsonApiEntity[],
  included?: IJsonApiEntity[],
  relationships?: object,
  meta: {
    total: number,
  }
}

function normalizeResponse<T>(data: IJsonApiResponse): T {
  const normalizedData: INormalizedData =  {
    data: data.data
  }

  if (data.meta && isNumber(data.meta.total)) {
    normalizedData.total = data.meta.total
  }
  console.log('Normalized JSON API response', normalizedData)
  return normalizedData as unknown as T
}

export default normalizeResponse
