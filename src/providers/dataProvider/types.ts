import { DataProvider } from 'react-admin'

export type ActionType =
  'getList' |
  'getOne' |
  'getMany' |
  'getManyReference' |
  'update' |
  'updateMany' |
  'create' |
  'delete' |
  'deleteMany'
  
declare interface IQuery {
    [key: string]: any,
  }
export interface IRequestParams {
  action: ActionType,
  path: string,
  query: IQuery,
  payload?: Record<string, any>,
}
