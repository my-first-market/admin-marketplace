import build from 'providers/dataProvider/builder'
import { imageApi } from '../paths'

export default build({
  path: imageApi,
})
