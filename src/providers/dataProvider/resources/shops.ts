import build from 'providers/dataProvider/builder'

export default build({
  include: 'shops',
})
