import { GetListParams, GetManyReferenceParams } from 'ra-core'

declare interface IQuery {
  [key: string]: any,
}

function prepareListParams(params: GetListParams | GetManyReferenceParams): IQuery {
  const { page, perPage } = params.pagination
  const query: IQuery = {}

  query['page'] = page
  query['perPage'] = perPage
  console.log(page, perPage)
  // query['start'] = start
  // query['end'] = end

  return query
}

export default prepareListParams
