import defaults from 'lodash/defaults'
import pick from 'lodash/pick'
import {
  CreateResult,
  DeleteManyResult,
  DeleteResult,
  GetListResult,
  GetManyReferenceResult,
  GetManyResult,
  GetOneResult,
  UpdateManyResult,
  UpdateResult,
} from 'ra-core'
import { DataProvider } from 'ra-core'
import { apiPath } from './paths'
import performRequest from './request'
import prepareListParams from './utils/prepareListParams'

interface IBuilderOptions {
  path?: typeof apiPath,
  include?: string,
  fields?: {
    [key: string]: string,
  },
  normalizePayload?: (data: any, prevData: any) => any,
  fetch?: typeof performRequest
}

const defaultOptions = {
  path: apiPath,
  include: null,
  fields: null,
  normalizePayload: (data: any) => data,
  fetch: performRequest,
}

function buildProvider(inputOptions?: IBuilderOptions): DataProvider {
  const options = defaults(inputOptions || {}, defaultOptions)
  const defaultQuery = pick(options, ['include', 'fields'])

  return {
    getList: (resource, params: any) => {
      return options.fetch<GetListResult>({
        action: 'getList',
        path: `${options.path('getList')}/${resource}`,
        query: { ...defaultQuery, ...prepareListParams(params) },
      })
    },
    getManyReference: (resource, params) => {
      return options.fetch<GetManyReferenceResult>({
        action: 'getManyReference',
        path: `${options.path('getManyReference')}/${resource}`,
        query: { ...defaultQuery, ...prepareListParams(params)  },
      })
    },
    getOne: (resource, params) => {
      return options.fetch<GetOneResult>({
        action: 'getOne',
        path: `${options.path('getOne')}/${resource}/${params.id}`,
        query: defaultQuery,
      })
    },
    delete: (resource, params) => {
      return options.fetch<DeleteResult>({
        action: 'delete',
        path: `${options.path('delete')}/${resource}/${params.id}`,
        query: defaultQuery,
      })
    },
    deleteMany: (resource) => {
      return options.fetch<DeleteManyResult>({
        action: 'deleteMany',
        path: `${options.path('deleteMany')}/${resource}`,
        query: defaultQuery,
      })
    },
    getMany: (resource, params) => {
      return options.fetch<GetManyResult>({
        action: 'getMany',
        path: `${options.path('getMany')}/${resource}`,
        query: { ...defaultQuery, filter: { id_in: params.ids } },
      })
    },
    create: (resource, params) => {
      return options.fetch<CreateResult>({
        action: 'create',
        path: `${options.path('create')}/${resource}`,
        query: defaultQuery,
        payload: options.normalizePayload(params.data),
      })
    },
    update: (resource, params) => {
      return options.fetch<UpdateResult>({
        action: 'update',
        path: `${options.path('update')}/${resource}/${params.id}`,
        query: defaultQuery,
        payload: options.normalizePayload(params.data, params.previousData),
        
      })
    },
    updateMany: (resource, params) => {
      return options.fetch<UpdateManyResult>({
        action: 'updateMany',
        path: `${options.path('updateMany')}/${resource}/update_many`,
        query: defaultQuery,
        payload: options.normalizePayload(params.data),
      })
    },
  }
}

export default buildProvider
