import { InvalidParamsError } from './errors'

async function handleError(res: Response) {
  const data = await res.json()
  if (!data.errors) {
    return data
  }
  const errors: IJsonApiError[] = data.errors
  throw new InvalidParamsError(errors[0].detail, errors)
}

export default handleError
