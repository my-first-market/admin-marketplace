import isEmpty from 'lodash/isEmpty'
import { stringify } from 'qs'
import handleError from './handleError'
import normalizeResponse from './normalizeResponse'
import { ActionType, IRequestParams } from './types'

const baseUrl = `${process.env.REACT_APP_API_PATH}/api`

declare interface IQuery {
  [key: string]: any,
}

function jsonApiHeaders() {
  const token = localStorage.getItem('token')
  return {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: token ? `Bearer ${token}` : '',
  }
}

function restApiHeaders() {
  const token = localStorage.getItem('token')
  return {
    'Content-Type': 'application/json',
    Authorization: token ? `Bearer ${token}` : '',
  }
}

function getRequestMethodByType(type: ActionType): string {
  switch (type) {
    case 'create': return 'POST'
    case 'delete':
    case 'deleteMany':
      return 'DELETE'
    case 'update':
      return 'PATCH'
    default: return 'GET'
  }
}

function makeFullPath(path: string, query: IQuery): string {
  const queryString = isEmpty(query) ? null : stringify(query, { arrayFormat: 'brackets', skipNulls: true })
  return isEmpty(query)
    ? `${baseUrl}${path}`
    : `${baseUrl}${path}?${queryString}`
}

function performRequest<T>({ action, path, query, payload }: IRequestParams): Promise<T> {
  const fullPath = makeFullPath(path, query)
  return fetch(fullPath, {
    method: getRequestMethodByType(action),
    headers: ['create', 'update'].includes(action)
    ? restApiHeaders()
    : jsonApiHeaders(),
    body: payload ? JSON.stringify(payload) : null,
  })
  .then(handleError)
  .then((data) => normalizeResponse<T>(data))
}

export {
  jsonApiHeaders,
  restApiHeaders,
  performRequest,
  getRequestMethodByType,
  makeFullPath,
}

export default performRequest
