class NotImplementedError extends Error {
  constructor(message: string) {
    super(message)

    this.message = message
    this.name = 'NotImplementedError'
  }
}

class InvalidParamsError extends Error {
  public errors: IJsonApiError[]

  constructor(message: string, errors: IJsonApiError[]) {
    super(message)

    this.message = message
    this.name = 'InvalidParamsError'
    this.errors = errors
  }
}

export {
  NotImplementedError,
  InvalidParamsError,
}
