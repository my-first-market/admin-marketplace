import login from "api/login"

interface IParams {
  username: string,
  password: string,
  success: boolean,
}

const authProvider = {
  login: (params: IParams) => login(params)
    .then((data: any) => {
      if (!data.success) {
        throw new Error(data.message);
      }
      localStorage.setItem('token', data.data.token)
      localStorage.setItem('permissions', data.data.user.role)
    }),
  logout: () => {
    localStorage.removeItem('token')
    localStorage.removeItem('permissions')
    return Promise.resolve()
  },
  checkAuth: () => (localStorage.getItem('token')
    ? Promise.resolve()
    : Promise.reject()),
  checkError: (error: any) => { console.error(error) },
  getPermissions: () => {
    const role = localStorage.getItem('permissions');
    return role ? Promise.resolve(role) : Promise.reject();
},
}

export default authProvider;