import defaultHeaders from './utils/defaultHeaders'
import handleError from './error'


function uploadPhotos(files: File[]): Promise<any> {
  console.log(files)
  const payload = new FormData()
  
  for (const file of files) {
    payload.append('images[]', file)
  }
  return fetch(`${process.env.REACT_APP_API_PATH}/api/images/upload`, {
    method: 'POST',
    headers: {
      Authorization: defaultHeaders().Authorization,
    },
    body: payload,
  })
    .then(handleError)
}

export default uploadPhotos
