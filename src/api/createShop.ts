import defaultHeaders from './utils/defaultHeaders'
import handleError from './error'
import pickBy from 'lodash/pickBy'

interface ICategoryParams {
  name: string,
  id: string,
}

function createShop(params: ICategoryParams) {
  const { name, id } = params
  return fetch(`${process.env.REACT_APP_API_PATH}/api/shops/${id}`, {
    method: 'POST',
    headers: defaultHeaders(),
    body: JSON.stringify(pickBy({ name })),
  })
  .then(handleError)
}

export default createShop
