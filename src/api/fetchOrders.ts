import castError from './error'
import defaultHeaders from './utils/defaultHeaders'

function fetchOrders() {
  return fetch(`${process.env.REACT_APP_API_PATH}/api/orders`, {
    method: 'GET',
    headers: defaultHeaders(),
  },
  )
  .then(castError)
  .then((res: any) => ({ data: res.data, meta: res.meta }))
}

export default fetchOrders
