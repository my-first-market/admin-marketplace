import defaultHeaders from './utils/defaultHeaders'
import handleError from './error'


function uploadFile(files: File[]): Promise<any> {
  const payload = new FormData()
  
  for (const file of files) {
    payload.append('images', file)
  }
  return fetch(`${process.env.REACT_APP_API_PATH}/api/import/images`, {
    method: 'POST',
    headers: {
      Authorization: defaultHeaders().Authorization,
    },
    body: payload,
  })
    .then(handleError)
}

export default uploadFile
