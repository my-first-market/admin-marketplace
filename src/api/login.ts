import handleError from 'api/error'

interface ILoginParams {
  username: string,
  password: string
}

function login({ username, password }: ILoginParams) {
  return fetch(`${process.env.REACT_APP_API_PATH}/api/auth/login`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ email: username, password }),
  })
  .then(handleError)
}

export default login
