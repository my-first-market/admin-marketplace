import defaultHeaders from './utils/defaultHeaders'
import handleError from './error'


function uploadFileCsv(files: File[]): Promise<any> {
  const payload = new FormData()
  
  for (const file of files) {
    payload.append('csv', file)
  }
  return fetch(`${process.env.REACT_APP_API_PATH}/api/import/csv`, {
    method: 'POST',
    headers: {
      Authorization: defaultHeaders().Authorization,
    },
    body: payload,
  })
    .then(handleError)
}

export default uploadFileCsv
