declare module 'react-admin'
declare module 'ra-jsonapi-client'
declare module 'ra-data-fakerest'
declare module 'redux-persist/lib/stateReconciler/autoMergeLevel2'
declare module 'redux-persist/lib/integration/react'
declare module 'react-admin-color-input'

declare interface IChoice {
  [key: string]: any
}

declare interface IQuery {
  [key: string]: any,
}

declare interface MatchingReferencesError {
  error: string
  filter: () => void
}

declare interface IRecord {
  id: string,
  _destroy: boolean,
}

declare interface IFilters {
  [key: string]: any
}

declare interface IListResourceProps<T> {
  basePath: string,
  data: T[],
  filterValues: IFilters
  setFilters: (values: IFilters) => any,
  history: History<HistoryLocationState>,
  location: Location,
}

declare interface IShowResourceProps<T> {
  basePath: string,
  data: T,
  record: T,
  resource: string,
  title: string,
  id: string | null,
  history: History<HistoryLocationState>,
  location: Location,
}

declare interface IFormProps<T> {
  basePath: string,
  data: T,
  record: T,
  resource: string,
  title: string,
  id: string | null,
  history: History<HistoryLocationState>,
  location: Location,
}

declare interface IQuery {
  [key: string]: any,
}

declare interface IJsonApiError {
  source: string,
  title: string,
  detail: string,
}

declare interface IDataProviderParams {
  id: number,
  ids: number[],
  pagination: {
    page: number,
    perPage: number,
  },
  filter?: {
    [key: string]: any,
  },
  sort?: {
    field: string,
    order: string,
  },
  data: {
    [key: string]: any,
  },
  previousData: {
    [key: string]: any,
  },
  target?: string,
}

declare type ArrayInputFieldsProp = {
  remove: (index: string) => any,
  push: (record: IRecord) => any,
  pop: () => IRecord | null,
  map: (any) => any[],
  length: number,
}

declare interface ISaveOptions {
  onSuccess?: () => void,
  onFailure?: () => void,
}

declare interface IPhoto extends IRecord {
  image: {
    url: string,
  }
}
