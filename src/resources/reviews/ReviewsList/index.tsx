import { List } from 'react-admin'
import React, { useState, useCallback } from 'react'
import { Datagrid, EditButton, TextField, DeleteButton, ReferenceField, DateField } from 'react-admin'
import DrawerReview from '../DrawerReview'
import ActiveField from 'components/ActiveField'
import { makeStyles } from '@material-ui/core/styles';

const useListStyles = makeStyles({
  comment: {
    maxWidth: '18em',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
});

const ReviewsList = (props: any) => {
  const classes = useListStyles();
  const [ selectedId, setSelectedId ] = useState<string | null>(null)
  const handleRowClick = useCallback((id: string) => {
    setSelectedId(id)
  }, [])

  return (
    <List {...props}>
      <Datagrid rowClick={(id: string) => handleRowClick(id)}>
        <DateField source='created_at' label='Дата создания' />
        <ReferenceField source="item_id" reference="products" label='Продукт'><TextField source="name" /></ReferenceField>
        <TextField source='rating' label='Количество звезд' />
        <TextField source='comment' label='Отзыв' cellClassName={classes.comment} />
        <ActiveField source='active' label='Статус' />
        <EditButton label='' />
        <DeleteButton label=''/>
        <DrawerReview orderId={selectedId} onClose={() => setSelectedId(null)} />
      </Datagrid>
    </List>
  )
}

export default ReviewsList
