import React from 'react'
import { Edit, TextField, BooleanInput, ReferenceField, NumberField, SimpleForm, TextInput } from 'react-admin'
import RichTextInput from 'ra-input-rich-text';


const ReviewsEdit = (props: any) => {
  return (
    <Edit {...props}>
      <SimpleForm {...props}>
        <ReferenceField source="item_id" reference="products" label='Продукт'><TextField source="name" /></ReferenceField>
        <ReferenceField source="user_id" reference="users"  label='Покупатель'><TextField source="name" /></ReferenceField>
        <TextInput multiline source="comment" label='Отзыв' />
        <NumberField source="rating" label='Количество звезд' />
        <BooleanInput source="active" label='Показывать' />
      </SimpleForm>
    </Edit>
  )
}

export default ReviewsEdit