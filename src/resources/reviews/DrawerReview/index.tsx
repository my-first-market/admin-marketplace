import { Drawer } from '@material-ui/core'
import React from 'react'
import ReviewsEdit from '../ReviewsEdit'

interface IProps {
  orderId: string | null,
  onClose: (orderId: string) => void,
}

const DrawerReview: React.FC<IProps> = ({ orderId, onClose }) => {
  return (
    <Drawer
      open={!!orderId}
      anchor='right'
      onClose={onClose}
      transitionDuration={600}
      ModalProps={{
        closeAfterTransition: true,
        onBackdropClick: (e: React.MouseEvent) => e.stopPropagation(),
      }}
    >
      { orderId &&
        <ReviewsEdit id={orderId} basePath='/reviews' resource='reviews' onClose={onClose} />
      }
    </Drawer>
  )
}

export default DrawerReview
