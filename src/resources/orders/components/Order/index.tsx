import React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Link, useTranslate, useQueryWithStore } from "react-admin";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  container: { minWidth: "35em", marginLeft: "1em" },
  rightAlignedCell: { textAlign: "right" },
  boldCell: { fontWeight: "bold" },
  total: { textAlign: "right", marginRight: 10, marginTop: 10 }
});

const Order = ({ record }: any) => {
  const classes = useStyles();
  const translate = useTranslate();
  console.log(record);
  const { loaded, data: products } = useQueryWithStore(
    {
      type: "getMany",
      resource: "products",
      payload: {
        ids: record ? record.productIds : []
      }
    },
    {},
    (state: any) => {
      const productIds = record ? record.productIds : [];
      return productIds
        .map((productId: any) => state.admin.resources.products.data[productId])
        .filter((r: any) => typeof r !== "undefined")
        .reduce((prev: any, next: any) => {
          prev[next.id] = next;
          return prev;
        }, {});
    }
  );
  if (!loaded || !record) return null;
  return (
    <Paper className={classes.container} elevation={2}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Изделие</TableCell>
            <TableCell className={classes.rightAlignedCell}>Цена</TableCell>
            <TableCell className={classes.rightAlignedCell}>
              Количество
            </TableCell>
            <TableCell className={classes.rightAlignedCell}>Сумма</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {record.productIds.map((id: any, index: number) => {
            const count = record.products[index].current_amount;
            const price = record.products[index].current_price;
            return (
              products[id] && (
                <TableRow key={id}>
                  <TableCell>
                    <Link to={`/products/${id}`}>{products[id].name}</Link>
                  </TableCell>
                  <TableCell className={classes.rightAlignedCell}>
                    {products[id].price}
                  </TableCell>
                  <TableCell className={classes.rightAlignedCell}>
                    {count}
                  </TableCell>
                  <TableCell className={classes.rightAlignedCell}>
                    {price * count}
                  </TableCell>
                </TableRow>
              )
            );
          })}
        </TableBody>
      </Table>
      <div className={classes.total}>
        {`Итоговая сумма: ${record.total_price} рублей`}
      </div>
    </Paper>
  );
};

export default Order;
