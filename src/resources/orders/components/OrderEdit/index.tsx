import React from 'react'
import { TextInput, Edit, SimpleForm, ReferenceInput, DateInput, SelectInput, TextField } from 'react-admin'
import Order from '../Order'

const choices = [
  // { id: 'pending', name: 'Проверка оплаты'},
  { id: 'active', name: 'Принят'},
  { id: 'disabled', name: 'Отменен'},
  // { id: 'deleted', name: 'Удален'},
  // { id: 'declined', name: 'Отказался'},
  { id: 'closed', name: 'Завершен'},
  // { id: 'paid', name: 'Оплачен'},
  // { id: 'sent', name: 'Готовится'},
  { id: 'delivered', name: 'В доставке'}

]
const OrderForm: React.FC<any> = (props) => (
  <Edit aside={<Order />} {...props}>
  <SimpleForm>
    <TextField source='id' label='Номер заказа' />
    {/* <TextField source='acquiring_id' label='Номер платежа' /> */}

      <DateInput source="created_at" readOnly={true} label='Дата заказа' />
      <SelectInput
        source='status'
        label='Статус'
        choices={choices}
      />
      <ReferenceInput label="Покупатель" source="user.id" reference="users">
        <SelectInput source="name" />
      </ReferenceInput>
      <TextInput source='description' label='Комментарий' multiline />
      {/* <TextField source='country' label='Страна'/>
      <TextField source='city' label='Город'/> */}
      <TextField source='address' label='Адрес'/>
      <TextField source='phone_mobile' label='Телефон'/>
  </SimpleForm>
</Edit>
)

export default OrderForm
