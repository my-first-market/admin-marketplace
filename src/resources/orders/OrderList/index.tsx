import { List } from 'react-admin'
import React from 'react'
import { Datagrid, TextField, DeleteButton, EditButton, ReferenceField, ReferenceArrayField, ReferenceManyField, SingleFieldList, ChipField, DateField } from 'react-admin'

const OrderList = (props: any) => (
  <List {...props}>
    <Datagrid>
      <DateField source="created_at" label='Дата заказа' />
      <TextField source='total_price' label='Сумма заказа' />
      <ReferenceField label="Покупатель" source="user.id" reference="users">
        <TextField source="name" />
      </ReferenceField>
      <ReferenceArrayField reference="products" source='productIds' label='Продукт'>
        <SingleFieldList>
          <ChipField source="name" />
        </SingleFieldList>
      </ReferenceArrayField>
      <EditButton label=''/>
      <DeleteButton label=''/>
    </Datagrid>
  </List>
)

export default OrderList
