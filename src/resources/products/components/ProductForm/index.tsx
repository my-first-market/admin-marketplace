import React from 'react'
import { TextInput, SimpleForm, ReferenceInput, AutocompleteInput, NumberInput, ImageField, BooleanInput } from 'react-admin'
import PhotoInput from 'components/PhotoInput'
import PhotoInputMulty from 'components/PhotoInputMulty'
import validator from '../../validator'
import RichInputText from 'ra-input-rich-text'

const optionRenderer = (choice: any) => {
  return (
    !choice ? '' : `${choice.name}`
  )
}

const ProductForm: React.FC<any> = (props) => (
  <SimpleForm {...props} validate={validator}>
    <PhotoInput multiple={false} source='logo' label='Главное фото'/>
    <ImageField source='logo.src' title='Logo Image' label='Главное фото' />
    <PhotoInputMulty source='gallery' title='Картинки' label='Картинки' />
    <ImageField source='gallery' src='src' label='Картинки' />
    <TextInput source='name' label='Имя'/>
    <TextInput multiline source='description' label='Краткое описание' />
    <RichInputText source='html' label='Описание' />
    <ReferenceInput label="Магазин" source="shop.id" reference="shops">
      <AutocompleteInput optionText={optionRenderer} />
    </ReferenceInput>
    <NumberInput source='price' label='Цена' />
    {/* <TextInput source='amount' label='Количество в наличии'/> */}
    <BooleanInput source='active' label='Показывать' />
    {/* <ReferenceInput label="Валюта" source="currency.id" reference="currency">
      <SelectInput optionText="name" />
    </ReferenceInput> */}
  </SimpleForm>
)

export default ProductForm
