import { List } from 'react-admin'
import React from 'react'
import { Datagrid, EditButton, TextField, DeleteButton } from 'react-admin'
import ActiveField from 'components/ActiveField'

const ProductsList = (props: any) => (
  <List {...props}>
    <Datagrid>
      <TextField source='name' label='Имя' />
      <TextField source='price' label='Цена' />
      {/* <TextField source='amount' label='Количество в наличии' /> */}
      <ActiveField source='active' label='Статус' />
      <EditButton label='' />
      <DeleteButton label=''/>
    </Datagrid>
  </List>
)

export default ProductsList
