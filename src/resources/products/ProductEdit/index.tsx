import React from 'react'
import { Edit } from 'react-admin'
import ProductForm from '../components/ProductForm/'

const ProductEdit = (props: any) => (
  <Edit {...props} >
    <ProductForm />
  </Edit>
)

export default ProductEdit
