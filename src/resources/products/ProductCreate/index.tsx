import React from 'react'
import { Create } from 'react-admin'
import ProductForm from '../components/ProductForm/'

const ProductCreate = (props: any) => (
  <Create {...props} >
    <ProductForm />
  </Create>
)

export default ProductCreate
