import { combineValidators, isRequired } from 'revalidate'

const customIsRequired = isRequired({ message: 'Обязательное поле' })

const productValidator = combineValidators({
  name: customIsRequired,
  price: customIsRequired,
  amount: customIsRequired,
})

export default productValidator
