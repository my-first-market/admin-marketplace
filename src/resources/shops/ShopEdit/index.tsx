import React from 'react'
import { Edit } from 'react-admin'
import ShopForm from '../components/ShopForm/ShopForm'

const ShopEdit = (props: any) => (
  <Edit {...props} >
    <ShopForm />
  </Edit>
)

export default ShopEdit
