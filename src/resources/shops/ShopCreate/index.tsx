import React from 'react'
import { Create } from 'react-admin'
import ShopForm from '../components/ShopForm/ShopForm'

const ShopCreate = (props: any) => (
  <Create {...props} >
    <ShopForm />
  </Create>
)

export default ShopCreate
