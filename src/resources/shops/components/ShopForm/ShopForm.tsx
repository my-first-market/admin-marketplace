import React from 'react'
import { TextInput, SimpleForm, ReferenceInput, SelectInput, BooleanInput, ImageField} from 'react-admin'
import PhotoInput from 'components/PhotoInput'
import HasRoles from 'utils/hasRoles'
import validator from '../../validator'

const ShopForm: React.FC<any> = (props) => (
  <SimpleForm {...props} validate={validator}>
    <PhotoInput multiple={false} source='logo' label='Главное фото'/>
    <ImageField source='logo.src' title='Logo Image' label='Главное фото' />
    <TextInput source='name' label='Название' />
    <HasRoles roles={['admin']}>
    <ReferenceInput label="Пользователь" source="user.id" reference="users">
      <SelectInput optionText="name" label='Имя' />
    </ReferenceInput>
    </HasRoles>
    <TextInput source='description' label='Описание' multiline />
    <TextInput source='contact.phone_mobile' label='Мобильный телефон' />
    <BooleanInput source='active' label='Активный' />
  </SimpleForm>
)

export default ShopForm
