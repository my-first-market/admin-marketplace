import { List } from 'react-admin'
import React from 'react'
import { Datagrid, EditButton, TextField, DeleteButton, usePermissions } from 'react-admin'
import ActiveField from 'components/ActiveField'

const ShopList = (props: any) => {
  const { loaded, permissions } = usePermissions()
  console.log(permissions)
return (
  <List {...props}>
    <Datagrid>
      <TextField source='name' label='Имя' />
      <TextField source='description' label='Описание' />
      <ActiveField source='active' label='Статус' />
      <EditButton label='' />
      <DeleteButton label=''/>
    </Datagrid>
  </List>
)
}

export default ShopList
