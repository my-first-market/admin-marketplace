import { combineValidators, isRequired } from 'revalidate'

const customIsRequired = isRequired({ message: 'Обязательное поле' })

const shopsValidator = combineValidators({
  name: customIsRequired,
})

export default shopsValidator
