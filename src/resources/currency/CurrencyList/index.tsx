import { List } from 'react-admin'
import React from 'react'
import { Datagrid, EditButton, TextField, DeleteButton } from 'react-admin'
import HasRoles from 'utils/hasRoles'

const CurrencyList = (props: any) => (
  <HasRoles roles={['admin']}>
    <List {...props}>
      <Datagrid>
        <TextField source='code' label='код' />
        <TextField source='name' label='имя' />
        <EditButton label='' />
        <DeleteButton label=''/>
      </Datagrid>
    </List>
   </HasRoles>
)

export default CurrencyList
