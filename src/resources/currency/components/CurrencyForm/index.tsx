import React from 'react'
import { TextInput, SimpleForm } from 'react-admin'
import HasRoles from 'utils/hasRoles'

const CurrencyForm: React.FC<any> = (props) => (
  <HasRoles roles={['admin']}>
    <SimpleForm {...props}>
      <TextInput source='code' label='код'/>
      <TextInput source='name' label='имя'/>
    </SimpleForm>
   </HasRoles>
)

export default CurrencyForm
