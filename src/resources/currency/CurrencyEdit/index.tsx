import React from 'react'
import { Edit } from 'react-admin'
import CurrencyForm from '../components/CurrencyForm'

const CurrencyEdit = (props: any) => (
  <Edit {...props} >
    <CurrencyForm />
  </Edit>
)

export default CurrencyEdit
