import React from 'react'
import { Create } from 'react-admin'
import CurrencyForm from '../components/CurrencyForm'

const CurrencyCreate = (props: any) => (
  <Create {...props} >
    <CurrencyForm />
  </Create>
)

export default CurrencyCreate
