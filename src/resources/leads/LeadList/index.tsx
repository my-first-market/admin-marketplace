import { List } from 'react-admin'
import React from 'react'
import { Datagrid, TextField, EmailField, ReferenceField, EditButton } from 'react-admin'

const LeadList = (props: any) => (
  <List {...props}>
      <Datagrid rowClick="edit">
          <TextField source="id" />
          <ReferenceField label="Покупатель" source="user.id" reference="users">
        <TextField source="name" />
      </ReferenceField>
          <TextField source="status" />
          <EmailField source="email" />
          <TextField source="phone" />
          <EditButton label='' />
      </Datagrid>
  </List>
);

export default LeadList