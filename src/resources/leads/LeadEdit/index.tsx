import React from 'react'
import { Edit, TextField, BooleanInput, ReferenceField, NumberField, SimpleForm, EmailField,  } from 'react-admin'

const LeadEdit = (props: any) => (
  <Edit {...props}>
    <SimpleForm {...props}>
    <TextField source="id" />
    <ReferenceField label="Покупатель" source="user.id" reference="users">
      <TextField source="name" />
    </ReferenceField>
    <EmailField source="email" />
    <TextField source="phone" />
    <BooleanInput source='active' label='Статус'/>
    </SimpleForm>
  </Edit>
)

export default LeadEdit