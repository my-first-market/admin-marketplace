import { combineValidators, isRequired } from 'revalidate'

const customIsRequired = isRequired({ message: 'Обязательное поле' })

const productsValidator = combineValidators({
  path_csv: customIsRequired,
  path_images: customIsRequired,
})

export default productsValidator
