import React from "react"
import { Create } from "react-admin"
import ImportProductForm from "../components/ImportProductForm"

const ImportProductCreate = (props: any) => (
  <Create {...props} >
    <ImportProductForm />
  </Create>
);

export default ImportProductCreate;
