import React, { useCallback } from "react";
import {
  SimpleForm,
  FileInput,
  FileField,
  ReferenceInput,
  SelectInput,
  AutocompleteInput
} from "react-admin";
import FileInputCustom from 'components/FileInputCustom'
import FileInputCsv from "components/FileInputCsv";
import validation from '../../validation'

const optionRenderer = (choice: any) => {
  return (
    !choice ? '' : `${choice.name}`
  )
}


const ImportProductForm: React.FC<any> = props => {

return (
  <SimpleForm {...props} validate={validation}>
    <ReferenceInput label="Магазин *" source="shop.id" reference="shops">
      <AutocompleteInput  optionText={optionRenderer} />
    </ReferenceInput>
    <FileInputCsv source='path_csv' csv accept='.csv' />

    <FileInputCustom source='path_images' accept='.zip' />
  </SimpleForm>
);
}

export default ImportProductForm;
