import { useEffect } from 'react'
import { useHistory } from 'react-router-dom'

const ImportList = () => {
  const history = useHistory()
  useEffect(() => {
    history.push('/products')
  }, [])
  return null
}

export default ImportList
