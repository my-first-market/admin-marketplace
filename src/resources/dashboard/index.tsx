import React, { useState, useEffect, useCallback } from 'react';
import { useVersion, useDataProvider } from 'react-admin';
import { useMediaQuery } from '@material-ui/core';
import AllOrders from './components/AllOrders'
import Monthly from './components/Monthly/';
import PendingOrders from './components/PendingOrders'
import fetchOrders from 'api/fetchOrders'
import useFetch from 'hooks/useFetch';
import { sumBy } from 'lodash';
import LineChart from './components/Chart';
import PieChart from './components/PieChart';

const styles = {
    flex: { display: 'flex' },
    flexColumn: { display: 'flex', flexDirection: 'column' },
    leftCol: { flex: 1, marginRight: '1em' },
    rightCol: { flex: 1, marginLeft: '1em' },
    singleCol: { marginTop: '2em', marginBottom: '2em', display: 'flex' },
};

const Dashboard = (props: any) => {
    const { data, fetch } = useFetch(fetchOrders);
    useEffect(() => {
      fetch();
    }, []);
    if(!data) return []
    const paidOrders = data.data.filter((order: any) => order.status === 'closed')
    const sum = sumBy(paidOrders, 'total_price')
    return  (
        <div style={styles.flex}>
            <div style={styles.leftCol}>
                <div style={styles.flex}>
                    <Monthly value={sum} />
                    <AllOrders value={data.data.length}/>
                </div>
                <div style={styles.singleCol}>
                    <PendingOrders
                        orders={data.data}
                    />
                    <LineChart orders={data.data} />
                </div>
                <div style={styles.singleCol}>
                    <PieChart orders={data.data} />
                </div>
            </div>
        </div>
    );
};

export default Dashboard;
