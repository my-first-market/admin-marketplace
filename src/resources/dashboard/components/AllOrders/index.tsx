import React from 'react';
import Card from '@material-ui/core/Card';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import CardIcon from '../CardIcon';

const useStyles = makeStyles({
    main: {
        flex: '1',
        marginLeft: '1em',
        marginTop: 20,
    },
    card: {
        overflow: 'inherit',
        textAlign: 'right',
        padding: 16,
        minHeight: 52,
    },
});

const NbNewOrders = ({ value }: any) => {
    const classes = useStyles();
    return (
        <div className={classes.main}>
            <CardIcon Icon={ShoppingCartIcon} bgColor="#ff9800" />
            <Card className={classes.card}>
                <Typography color="textSecondary">
                  Всего заказов
                </Typography>
                <Typography variant="h5" component="h2">
                    {value}
                </Typography>
            </Card>
        </div>
    );
};

export default NbNewOrders;
