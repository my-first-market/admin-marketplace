import React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    root: {
        flex: 1,
        marginRight: '1em',
    },
    cost: {
        marginRight: '1em',
        color: theme.palette.text.primary,
    },
}));

const PendingOrders = ({ orders }: any) => {
    const classes = useStyles();
    console.log(orders)
    return (
        <Card className={classes.root}>
            <CardHeader title='Последние заказы' />
            <List dense={true} style={{maxHeight: '300px', overflow: 'auto'}}>
                {orders.map((order: any) => (
                    <ListItem
                        button
                        component={Link}
                        to={`/orders/${order.id}`}
                    >
                        <ListItemAvatar>
                            <Avatar src={order.user.logo && order.user.logo.src} /> 
                        </ListItemAvatar>
                        <ListItemText
                            primary={`${order.user.name}`}
                        />
                        
                        <ListItemSecondaryAction>
                            <span className={classes.cost}>
                                {order.total_price}
                            </span>
                        </ListItemSecondaryAction>
                    </ListItem>
                ))}
            </List>
        </Card>
    );
};

export default PendingOrders;
