import React, {Component} from 'react';
import { Line } from 'react-chartjs-2';
import moment from 'moment'
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';

const useStyles = makeStyles(theme => ({
  root: {
      flex: 1,
      marginLeft: '1em',

  },
  cost: {
      marginRight: '1em',
      color: theme.palette.text.primary,
  },

}));
const LineChart = ({orders}: any) => {
  const classes = useStyles();
  const months = orders.map((order: any) => moment(order.created_at).format('ll')).reverse()
  const totalPrices = orders.map((order: any) => order.total_price).reverse()
  const data = {
    labels: months,
    datasets: [
      {
        label: 'Оборот',
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'rgba(75,192,192,0.4)',
        borderColor: '#3f50b5',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: '#3f50b5',
        pointBackgroundColor: '#3f50b5',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: '#3f50b5',
        pointHoverBorderColor: '#3f50b5',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: totalPrices
      }
    ]
  };
    return (
      <Card className={classes.root}>
        <CardHeader title='Оборот по заказам' />
        <Line data={data} />
      </Card>
    );
  }

export default LineChart