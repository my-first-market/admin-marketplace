import React from 'react';
import Card from '@material-ui/core/Card';
import DollarIcon from '@material-ui/icons/AttachMoney';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { useTranslate } from 'react-admin';

import CardIcon from '../CardIcon'

const useStyles = makeStyles({
    main: {
        flex: '1',
        marginRight: '1em',
        marginTop: 20,
    },
    card: {
        overflow: 'inherit',
        textAlign: 'right',
        padding: 16,
        minHeight: 52,
    },
});

const MonthlyRevenue = ({ value }: any) => {
    const classes = useStyles();
    return (
        <div className={classes.main}>
            <CardIcon Icon={DollarIcon} bgColor="#31708f" />
            <Card className={classes.card}>
                <Typography color="textSecondary">
                    Выручка
                </Typography>
                <Typography variant="h5" component="h2">
                    {value}
                </Typography>
            </Card>
        </div>
    );
};

export default MonthlyRevenue;
