import React from 'react';
import {Pie} from 'react-chartjs-2';
import { groupBy, get } from 'lodash';
import humanizeStatus from 'utils/humanizeStatus';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
      flex: 1,
      paddingBottom: 20,
  },
  cost: {
      color: theme.palette.text.primary,
  },

}));

const PieChart = ({orders}: any) => {
  const classes = useStyles();
  const group = groupBy(orders, 'status')
  const names = Object.keys(group).map((name) => humanizeStatus(name))
  const lenghts = Object.keys(group).map((name) => get(group, name).length)
  const data = {
    labels: names,
    datasets: [{
      data: lenghts,
      backgroundColor: [
      '#FF6384',
      '#36A2EB',
      '#FFCE56'
      ],
      hoverBackgroundColor: [
      '#FF6384',
      '#36A2EB',
      '#FFCE56'
      ]
    }]
  };
  
    return (
      <Card className={classes.root}>
        <CardHeader title='Cтатусы заказов' />
        <Pie data={data} />
      </Card>
    );
  };

export default PieChart;