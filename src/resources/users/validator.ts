import { combineValidators, isRequired } from 'revalidate'

const customIsRequired = isRequired({ message: 'Обязательное поле' })

const userValidator = combineValidators({
  name: customIsRequired,
  email: customIsRequired,
})

export default userValidator
