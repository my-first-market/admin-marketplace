import React from 'react'
import { TextInput, SimpleForm, PasswordInput, ImageField, SelectInput, BooleanInput } from 'react-admin'
import PhotoInput from 'components/PhotoInput'
import HasRoles from 'utils/hasRoles'
import validator from '../validator'
import CustomImageField from 'components/CustomImageField'

const choices = [
  { id: 'user', name: 'Покупатель'},
  { id: 'admin', name: 'Администратор'},
  { id: 'seller', name: 'Менеджер'},
];

const UserForm: React.FC<any> = (props) => (
  <HasRoles roles={['admin']}>
    <SimpleForm {...props} validate={validator}>
      <PhotoInput source='logo' label='Аватар' />
      <CustomImageField source='logo' />
      <TextInput source='name' label='Имя' />
      <SelectInput source='role' label='Роль' choices={choices} optionText='name' optionValue='id' />
      <TextInput source='email' autocomplete={false} label='Почта' />
      <PasswordInput source='password' autocomplete={false} label='Пароль' />
      <TextInput source='contact.phone_mobile' label='Мобильный телефон' />
      {/* <TextInput source='contact.phone_home' label='Рабочий телефон' /> */}
      {/* <TextInput source='contact.country' label='Страна' />
      <TextInput source='contact.city' label='Город' /> */}
      {/* <TextInput source='contact.address' label='Адрес' /> */}
      <BooleanInput source='active' label='Активный' />
    </SimpleForm>
  </HasRoles>
)

export default UserForm
