import React from 'react'
import { Create } from 'react-admin'
import UserForm from '../components/UserForm'
const UserCreate = (props: any) => (
  
  <Create {...props} >
    <UserForm />
  </Create>
)

export default UserCreate
