import React from 'react'
import { Edit } from 'react-admin'
import UserForm from '../components/UserForm'
const UserEdit = (props: any) => (
  <Edit {...props} >
    <UserForm />
  </Edit>
)

export default UserEdit
