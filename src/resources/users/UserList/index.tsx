import { List } from 'react-admin'
import React from 'react'
import { Datagrid, EditButton, TextField, DeleteButton } from 'react-admin'
import HasRoles from 'utils/hasRoles'
import ActiveField from 'components/ActiveField'

const UserList = (props: any) => (
  <HasRoles roles={['admin']}>
    <List {...props}>
      <Datagrid>
        <TextField source='name' label='Имя' />
        <TextField source='email' label='Почта' />
        {/* <TextField source='role' label='Роль' /> */}
        <ActiveField source='active' label='Статус' />
        <EditButton label=''/>
        <DeleteButton label='' />
      </Datagrid>
    </List>
   </HasRoles>
)

export default UserList
