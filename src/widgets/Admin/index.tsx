import { createMuiTheme } from '@material-ui/core/styles'
import { authProvider, dataProvider } from 'providers'
import React from 'react'
import { Admin as AdminComponent, ResettableTextField } from 'react-admin'
import Layout from 'widgets/Layout'
import {russian} from 'locale'
import polyglotI18nProvider from 'ra-i18n-polyglot'
import Dashboard from 'resources/dashboard'
import themeReducer from 'resources/configuration/reducer'
import { Route } from 'react-router-dom'
import Configuration from 'resources/configuration'

ResettableTextField.defaultProps = {
  variant: 'outlined',
}

const i18nProvider = polyglotI18nProvider(() => russian, 'ru');




const theme = createMuiTheme({
  overrides: {
    MuiTableCell: {
      head: {
        fontSize: '1.1rem',
      },
    },
    MuiAppBar: {
      colorSecondary: {
          backgroundColor: '#000',
      },
    },
  },
})

interface IProps {
  children: React.ReactNode,
}
const customRoutes = [
  <Route exact path='/configuration' component={Configuration} />,
]

const Admin: React.FC<IProps> = ({ children }) => (
  <AdminComponent
    locale='ru'
    i18nProvider={i18nProvider}
    theme={theme}
    dataProvider={dataProvider}
    authProvider={authProvider}
    layout={Layout}
    dashboard={Dashboard}
    customReducers={{ theme: themeReducer }}
    customRoutes={customRoutes}
    // history={history}
  >
    {children}
  </AdminComponent>
)

export default Admin
