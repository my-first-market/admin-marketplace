import React, { Component } from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import StorefrontIcon from "@material-ui/icons/Storefront";
import LocalGroceryStoreIcon from "@material-ui/icons/LocalGroceryStore";
import WorkIcon from "@material-ui/icons/Work";
import { MenuItemLink, DashboardMenuItem } from "react-admin";
import PeopleIcon from "@material-ui/icons/People";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";
import HasRoles from "utils/hasRoles";
import SettingsIcon from "@material-ui/icons/Settings";
import ReviewIcon from "@material-ui/icons/Comment";
import EmojiPeopleIcon from "@material-ui/icons/EmojiPeople";
import LabelImportantIcon from '@material-ui/icons/LabelImportant';

interface IProps extends RouteComponentProps {
  onMenuClick: (event: any) => void;
  dense: boolean;
  open: boolean;
  theme: string;
  translate: (key: string, options?: object) => string;
  restaurantId: string;
}

const routes = {
  shops: "/shops",
  users: "/users",
  products: "/products",
  currency: "/currencies",
  orders: "/orders",
  configuration: "/configuration",
  reviews: "/reviews",
  leads: "/leads",
  importProducts: '/import/products/create',
};

const Menu = ({ onMenuClick, open, dense }: IProps) => {
  return (
    <div>
      <DashboardMenuItem onClick={onMenuClick} sidebarIsOpen={open} />
      {/* <HasRoles roles={["admin"]}>
      <MenuItemLink
        to={routes.shops}
        primaryText="Магазины"
        leftIcon={<StorefrontIcon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
        dense={dense}
      />
      </HasRoles> */}
      <MenuItemLink
        to={routes.products}
        primaryText="Продукция"
        leftIcon={<WorkIcon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
        dense={dense}
      />
      <MenuItemLink
        to={routes.importProducts}
        primaryText="Импорт продукции"
        leftIcon={<LabelImportantIcon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
        dense={dense}
      />
      <HasRoles roles={["admin"]}>
        <>
          <MenuItemLink
            to={routes.users}
            primaryText="Пользователи"
            leftIcon={<PeopleIcon />}
            onClick={onMenuClick}
            sidebarIsOpen={open}
            dense={dense}
          />
          {/* <MenuItemLink
            to={routes.currency}
            primaryText="Валюты"
            leftIcon={<AttachMoneyIcon />}
            onClick={onMenuClick}
            sidebarIsOpen={open}
            dense={dense}
          /> */}
        </>
      </HasRoles>
      <MenuItemLink
        to={routes.orders}
        primaryText="Заказы"
        leftIcon={<LocalGroceryStoreIcon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
        dense={dense}
      />
      <MenuItemLink
        to={routes.reviews}
        primaryText="Отзывы"
        leftIcon={<ReviewIcon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
        dense={dense}
      />
      {/* <HasRoles roles={["admin"]}>
      <MenuItemLink
        to={routes.leads}
        primaryText="Заявки"
        leftIcon={<EmojiPeopleIcon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
        dense={dense}
      />
      </HasRoles> */}

      <MenuItemLink
        to={routes.configuration}
        primaryText="Настройки"
        leftIcon={<SettingsIcon />}
        onClick={onMenuClick}
        sidebarIsOpen={open}
        dense={dense}
      />
    </div>
  );
};

export default Menu;
