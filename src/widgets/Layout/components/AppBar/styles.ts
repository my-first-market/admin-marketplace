import { createStyles } from '@material-ui/core/styles'

const styles = createStyles({
  title: {
    flex: 1,
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
  },
  spacer: {
      flex: '1',
  },
  header: {
    background: 'linear-gradient(145deg,#ffa500 11%,#ffa500 75%)'
  },
  logo: {
    fontFamily: 'Arial',
    fontWeight: 'bold',
    fontSize: '1.5rem',
    color: '#fff',
    paddingLeft: '.5em',
  },
})

export default styles
