import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import React from 'react'
import { AppBar as DefaultAppBar } from 'react-admin'
import styles from './styles'

interface IProps {
  classes: {
    title: string,
    spacer: string,
    header: string,
    logo: string,
  }
}

const AppBar = ({ classes, ...props }: IProps) => (
    <DefaultAppBar {...props} className={classes.header}>
      <span className={classes.logo}>Hot bread admin</span>
      <span className={classes.spacer} />
      <Typography
          variant='caption'
          color='inherit'
          className={classes.title}
      />
      <span className={classes.spacer} />
    </DefaultAppBar>
)

export default withStyles(styles)(AppBar)
