import React from 'react'
import { Layout as LayoutComponent } from 'react-admin'
import AppBar from './components/AppBar'
import Menu from './components/Menu'
import { darkTheme, lightTheme } from './components/themes';
import { useSelector } from 'react-redux';

const Layout: React.FC = (props: unknown) => {

  const theme = useSelector((state: any) =>
    state.theme === 'dark' ? darkTheme : lightTheme
  );
  
  return (
  <LayoutComponent
    {...props}
    menu={Menu}
    appBar={AppBar}
    theme={theme}
  />
)}

export default Layout
